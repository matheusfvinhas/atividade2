package br.gov.sp.fatec.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.gov.sp.fatec.model.Filme;

public interface FilmeRepository extends CrudRepository<Filme, Long>{

	public Filme findByNome(String nome);
	
	public List<Filme> findByDiretor(String diretor);
	
	public List<Filme> findByGenero(String genero);	
	
	
}
