package br.gov.sp.fatec;

import java.util.ArrayList;


import org.springframework.context.support.ClassPathXmlApplicationContext;

import br.gov.sp.fatec.model.Filme;
import br.gov.sp.fatec.model.Usuario;
import br.gov.sp.fatec.repository.FilmeRepository;
import br.gov.sp.fatec.repository.UsuarioRepository;
import br.gov.sp.fatec.service.AppService;

public class App {
	

	public static void main(String[] args) { 
		ClassPathXmlApplicationContext context =
			new ClassPathXmlApplicationContext("applicationContext.xml");
		FilmeRepository filmeRepo = (FilmeRepository) context.getBean("filmeRepository");
		UsuarioRepository usuarioRepo = (UsuarioRepository) context.getBean("usuarioRepository");		
		
		Filme filme1 = new Filme();
		filme1.setNome("Clube da Luta");
		filme1.setDiretor("David Fincher");
		filme1.setGenero("Drama");
		filme1.setAtores("Brad Pitt, Edward Norton, Meat Loaf");
		filme1.setRoteirista("Chuck Palahniuk, Jim Uhls");
		
		filmeRepo.save(filme1);
		
		Filme filme2 = new Filme();
		filme2.setNome("Pulp Fiction");
		filme2.setDiretor("Quentin Tarantino");
		filme2.setGenero("Drama");
		filme2.setAtores("John Travolta, Uma Thurman, Samuel L. Jackson");
		filme2.setRoteirista("Quentin Tarantino, Roger Avary");
		
		filmeRepo.save(filme2);
		
		Usuario user = new Usuario();
		user.setNome("Matheus");
		user.setSenha("senha123");
		user.setFilmes(new ArrayList<Filme>());
		user.getFilmes().add(filme1);
		user.getFilmes().add(filme2);
		
		usuarioRepo.save(user);
		
		for (Filme filme : user.getFilmes()) {
			System.out.println(filme.toString());
		}
		
		System.out.println(filmeRepo.findByNome("Clube da Luta"));
		
		for (Filme filme : filmeRepo.findByDiretor("David Fincher")){
			System.out.println(filme.getNome());
		}
		
		for (Filme filme : filmeRepo.findByGenero("Drama")){
			System.out.println(filme.getNome());
		}
				
		
		AppService app = (AppService)context.getBean("appService");
		
		try {
			app.InserirFilme();
		}
		catch(Exception e) {
			System.out.println("Erro esperado! Rollback realizado!");
			e.printStackTrace();
		}
		
		usuarioRepo.delete(user);
		
		filmeRepo.delete(filme1);
		filmeRepo.delete(filme2);
		
		
		
	}
}
