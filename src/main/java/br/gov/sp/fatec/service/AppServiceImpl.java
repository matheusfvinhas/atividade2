package br.gov.sp.fatec.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.gov.sp.fatec.model.Filme;


import br.gov.sp.fatec.repository.FilmeRepository;

@Service("appService")
public class AppServiceImpl implements AppService {
		
	@Autowired
	private FilmeRepository filmeRepo;
		
	public void setFilmeRepo(FilmeRepository filmeRepo) {
		this.filmeRepo = filmeRepo;
	}
	
	@Transactional
	public void InserirFilme() {
		Filme filme1 = new Filme();
		filme1.setNome("Clube da Luta");
				
		filmeRepo.save(filme1);		
		
	}

}

